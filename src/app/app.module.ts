import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ServicesComponent } from './components/servicios/services.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { NavbarComponent } from './includes/navbar/navbar.component';
import { ModalLoginComponent } from './includes/modal-login/modal-login.component';
import { FooterComponent } from './includes/footer/footer.component';
import { NavbarSecComponent } from './includes/navbar-sec/navbar-sec.component';
import { SidebarAdminComponent } from './includes/sidebar-admin/sidebar-admin.component';
import { SliderComponent } from './components/slider/slider.component';
import { EmpresasComponent } from './components/empresas/empresas.component';
import { CategoriasHomeComponent } from './components/categorias-home/categorias-home.component';
import { HomeInicioComponent } from './components/home-inicio/home-inicio.component';
import { AdminPrincComponent } from './components/admin-princ/admin-princ.component';
import { TableroComponent } from './components/tablero/tablero.component';
import { CompanyComponent } from './components/company/company.component';
import { UsersComponent } from './components/users/users.component';
import { CategoryComponent } from './components/category/category.component';
import { RegisterCompanyComponent } from './components/register-company/register-company.component';
import { ContactUserComponent }from './components/contact-user/contact-user.component';
//Firebase/

import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorageModule, StorageBucket } from '@angular/fire/storage';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from 'src/environments/environment';
import { storage } from 'firebase';
import { ModRegUserComponent } from './includes/mod-reg-user/mod-reg-user.component';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormsModule } from '@angular/forms';
import { ReadCompanyComponent } from './components/read-company/read-company.component';
import { AboutComponent } from './components/about/about.component';

// Consumir API
import { HttpClientModule } from "@angular/common/http";
import { ApiServiceService } from './services/api-service.service';
import { ModalCategoriasComponent } from './components/modal-categorias/modal-categorias.component';
import { TablaclienteComponent } from './components/tablacliente/tablacliente.component';
import { TablaadministradorComponent } from './components/tablaadministrador/tablaadministrador.component';
import { TablaempresaComponent } from './components/tablaempresa/tablaempresa.component';
import { ModalinfoempresaComponent } from './components/modalinfoempresa/modalinfoempresa.component';



import { AgmCoreModule } from '@agm/core';
import { GuiasComponent } from './components/guias/guias.component';
import { UserMenuComponent } from './components/user-menu/user-menu.component';
import { UserPanelComponent } from './components/user-panel/user-panel.component';
import { FavoritosComponent } from './components/favoritos/favoritos.component';
import { GestionarEmpresaComponent } from './components/gestionar-empresa/gestionar-empresa.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { ModalagregarserviciosComponent } from './components/modalagregarservicios/modalagregarservicios.component';
import { ModalagregarsucursalComponent } from './components/modalagregarsucursal/modalagregarsucursal.component';
import { ModalfavoritosComponent } from './components/modalfavoritos/modalfavoritos.component';


//services
import {ClienteService} from './services/cliente.service';
import { CategoriavistaclienteComponent } from './components/categoriavistacliente/categoriavistacliente.component';
import { QRCodeModule } from 'angularx-qrcode';
import { ModalQrComponent } from './components/modal-qr/modal-qr.component';
import { ModalCoinsComponent } from './components/modal-coins/modal-coins.component';
import { EmpCatPipe } from './pipes/emp-cat.pipe';
import { PipeEmpresasPipe } from './pipes/pipe-empresas.pipe'

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ServicesComponent,
    ContactoComponent,
    NavbarComponent,
    ModalLoginComponent,
    FooterComponent,
    NavbarSecComponent,
    SidebarAdminComponent,
    SliderComponent,
    EmpresasComponent,
    CategoriasHomeComponent,
    HomeInicioComponent,
    AdminPrincComponent,
    TableroComponent,
    CompanyComponent,
    UsersComponent,
    CategoryComponent,
    RegisterCompanyComponent,
    ModRegUserComponent,
    ReadCompanyComponent,
    AboutComponent,
    ModalCategoriasComponent,
    TablaclienteComponent,
    TablaadministradorComponent,
    TablaempresaComponent,
    ModalinfoempresaComponent,
    GuiasComponent,
    UserMenuComponent,
    UserPanelComponent,
    FavoritosComponent,
    GestionarEmpresaComponent,
    ClientesComponent,
    ModalagregarserviciosComponent,
    ModalagregarsucursalComponent,
    ModalfavoritosComponent,
    PipeEmpresasPipe,
    CategoriavistaclienteComponent,
    ContactUserComponent,
    ModalQrComponent,
    ModalCoinsComponent,
    EmpCatPipe
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    FormsModule,
    HttpClientModule,
    QRCodeModule,
    AgmCoreModule.forRoot({
      // please get your own API key here:
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en
      apiKey: 'AIzaSyCn0FBsw-7VHdkGtuW1GdE-qeLr6ui3JCk'    })

    
  ],
  providers: [ClienteService,AngularFireAuth, AngularFirestore,ApiServiceService,
    {provide: StorageBucket, useValue: 'gs://prueba-435d6.appspot.com'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }