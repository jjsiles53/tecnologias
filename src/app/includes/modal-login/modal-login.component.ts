import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

import swal from 'sweetalert';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-modal-login',
  templateUrl: './modal-login.component.html',
  styleUrls: ['./modal-login.component.scss']
})
export class ModalLoginComponent implements OnInit {

  constructor(public afAuth: AngularFireAuth, private router: Router, private authService: AuthService, private cookie: CookieService) { }
  @ViewChild('exampleModal', {static: false}) btnClose: ElementRef;

  public email: string = '';
  public password: string = '';
  
  ngOnInit() {
  }

  onLogin(): void{
    this.authService.loginUser(this.email, this.password)
    .then((res) => {
      swal("Logeo correcto!", "Bienvenido!", "info");

      this.router.navigate(['/admin/company'])
    }).catch( err => {
      console.log('err', err.mesage);
      swal("Contraseña o nombre de usuario equivocada!", "intente de nuevo!", "warning");
    });

    document.getElementById('exampleModal').click();
    

  }

  onLogout() {
    var r = confirm("¿Esta seguro que quieres salir?");

    if (r == true) { 
      swal("Se deslogeo correctamente!", "Vuelva pronto!", "info");
      this.authService.logoutUser();

    }else{
      swal("Se deslogeo Cancelado!", "Bienvenido otra vez!", "info");

    }

  }

}
