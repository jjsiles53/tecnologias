import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import swal from 'sweetalert';
import { DataApiService } from 'src/app/services/data-api.service';
import { CookieService } from 'ngx-cookie-service';
import { ApiServiceService } from 'src/app/services/api-service.service';



@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(private api_service: ApiServiceService, private authService: AuthService, private afsAuth: AngularFireAuth, private router: Router, private data_api:DataApiService, private coockieservice: CookieService) { }

  empresa:any;
  public usuariobd: any;
  public tipo_u: number = 0;
  public nombre: string = "";
  public apellidos: string = "";
  public islogged: boolean = false;
  public usuario: any={nombre:"", apellido:"", correo_electronico: "", celular: "", clave: "",token: "", tipo_usuario: 1} ;
  

  ngOnInit() {
    this.getCurrentUser();
    
  }

  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if(auth) {
        
        // console.log('user logged',auth.uid);
       this.data_api.read("Usuario", auth.uid).subscribe(result=>{
         this.get_empresa();
         this.usuario = result.payload.data();
         this.coockieservice.set("id_usuario",auth.uid);

         this.get_cliente();
        // console.log("Este es el usuaario",this.usuario);
       });
        
        

        this.islogged = true;
      }
        else {
          // console.log('Not user logged');
          this.islogged = false;
        }
    })
  }

  onLogout() {
    var r = confirm("¿Esta seguro que quieres salir?");

    if (r == true) { 
      swal("Se deslogeo correctamente!", "Vuelva pronto!", "info");
      this.coockieservice.delete("id_usuario");
    this.authService.logoutUser();
    
    this.router.navigate(['home']);
    }else{
      swal("deslogeo Cancelado!", "Bienvenido otra vez!", "info");

    }
  }
  get_empresa() {
    var id_usaurio = this.coockieservice.get("id_usuario");
    this.api_service.get_emp_id_user(id_usaurio).subscribe(resp => {
      this.empresa = resp;
      for (const key in this.empresa) {
        if (this.empresa.hasOwnProperty(key)) {
          const element = this.empresa[key];
          // console.log("Element",element["id_empresa"]);
          this.coockieservice.set("id_empresa", element["id_empresa"]);


        }
      }
      // this.coockie_service.set("id_empresa",this.empresa.)

      console.log("esta es la empresa", resp);

    }, error => {
      console.log("Error", error);

    });
  }

  get_cliente(){
    this.data_api.read("Usuario",this.coockieservice.get("id_usuario") ).subscribe(data => {
      console.log(data,"este es el usuario");
    this.usuariobd = data.payload.data();
    this.tipo_u = this.usuariobd.tipo_usuario;
    this.apellidos = this.usuariobd.apellido;
    this.nombre = this.usuariobd.nombre;
    this.coockieservice.set("tipoUser", this.tipo_u.toString());
    this.coockieservice.set("name", this.nombre);
    this.coockieservice.set("lastn", this. apellidos);
  }, (err: any) => {
      console.error(err);      
    }
    );

  }

}
