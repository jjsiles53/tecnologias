import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModRegUserComponent } from './mod-reg-user.component';

describe('ModRegUserComponent', () => {
  let component: ModRegUserComponent;
  let fixture: ComponentFixture<ModRegUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModRegUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModRegUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
