import { Component, OnInit } from '@angular/core';
import { DataApiService } from 'src/app/services/data-api.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { ApiServiceService } from 'src/app/services/api-service.service';



@Component({
  selector: 'app-mod-reg-user',
  templateUrl: './mod-reg-user.component.html',
  styleUrls: ['./mod-reg-user.component.scss']
})
export class ModRegUserComponent implements OnInit {

  constructor(private data_api: DataApiService, private auth: AuthService, private router: Router, private api_service: ApiServiceService) { }
  tipo_usuario: any;
  user: any;
  usuario: any = { nombre: "", apellido: "", correo_electronico: "", celular: "", clave: "", token: "", tipo_usuario: 1 };
  usuario_vitacora: any = {nombre: "", apellido:""};
  bandera: any;

  ngOnInit() {
  }
  registrar_usuario() {

    if (this.tipo_usuario) {
      this.usuario.tipo_usuario = 2;
      this.api_service.get_emp_token(this.usuario.token).subscribe(resp => {
        this.bandera = resp;
        console.log(this.bandera);

      });
      if (this.bandera) {        
        this.auth.registerUser(this.usuario.correo_electronico, this.usuario.clave).then(respuesta => {
          this.user = respuesta;
          this.data_api.create(this.usuario, "Usuario", this.user.user.uid).then(resp => {
            this.api_service.put_empresa_idpersona(this.user.user.uid, this.usuario).subscribe(resp => {
              console.log(resp);
            });
            this.usuario.nombre = "";
            this.usuario.apellido = "";
            this.usuario.correo_electronico = "";
            this.usuario.celular = "";
            this.usuario.clave = "";
            this.usuario.token = "";
            this.user.user.uid = "";
            this.usuario.tipo_usuario = null;
          });
          
          $("[data-dismiss=modal]").trigger({ type: "click" });

          this.router.navigate(['/admin/company']);
        });

      }

    } else {
      this.usuario.token = "";
      this.auth.registerUser(this.usuario.correo_electronico, this.usuario.clave).then(respuesta => {
        this.user = respuesta;
        console.log(this.user.user.uid, "Este es el id definitivo");
        this.data_api.create(this.usuario, "Usuario", this.user.user.uid).then(resp => {
          console.log("Estoy aqui!");
          this.registrar_vitacora(this.usuario.nombre,this.usuario.apellido);
          this.usuario.nombre = "";
          this.usuario.apellido = "";
          this.usuario.correo_electronico = "";
          this.usuario.celular = "";
          this.usuario.clave = "";
          this.usuario.token = "";
          this.user.user.uid = "";
          this.usuario.tipo_usuario = null;
        });
        $("[data-dismiss=modal]").trigger({ type: "click" });

        // this.router.navigate(['tablero']);
      });
    }


  }

  registrar_vitacora(nombre, apellido){
    this.usuario_vitacora.nombre = nombre;
    this.usuario_vitacora.apellido = apellido;
    console.log("Vitacora:",this.usuario_vitacora);
    this.api_service.add_all(this.usuario_vitacora,"vitacora/nuevo").subscribe(response=>{
      console.log("Insercion Completada", response);
      this.usuario_vitacora =  {nombre_categoria:"",descripcion_categoria : ""};
    },
    error =>{
      console.log(error); 
    });
    
  }


}
