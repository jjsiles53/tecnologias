import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import swal from 'sweetalert';
import { CookieService } from 'ngx-cookie-service';
import { UserInterface } from '../../modelos/user';
import { DataApiService } from 'src/app/services/data-api.service';
import { QRCodeModule } from 'angularx-qrcode';


@Component({
  selector: 'app-sidebar-admin',
  templateUrl: './sidebar-admin.component.html',
  styleUrls: ['./sidebar-admin.component.scss']
})
export class SidebarAdminComponent implements OnInit {

  public myAngularxQrCode: string = null;

  constructor(private authService: AuthService, private afsAuth: AngularFireAuth, private router: Router, private coockieservice: CookieService, private data_api:DataApiService) {
    this.myAngularxQrCode = this.coockieservice.get('id_usuario');
   }

  public tipo_u: number = 0;
  public usuariobd:any;
  public isAdmin: any = null;
  public userUid: string = null;
  public islogged: boolean = false;

  ngOnInit() {
     this.getCurrentUser();
     this.menu();
     //this.get_cliente();
  }
    menu() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
     }
   else {
         x.className = "topnav";
     }
     console.log(x.className);
 }
  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if(auth) {
        // console.log('user logged');
        this.islogged = true;
      }
        else {
          // console.log('Not user logged');
          this.islogged = false;
        }
    })
  }

  onLogout() {
    var r = confirm("¿Esta seguro que quieres salir?");

    if (r == true) { 
      swal("Se deslogeo correctamente!", "Vuelva pronto!", "info");
      this.coockieservice.delete("id_usuario");
    this.authService.logoutUser();
    this.router.navigate(['home']);
    }else{
      swal("deslogeo Cancelado!", "Bienvenido otra vez!", "info");

    }
  }

  // get_cliente(){
  //   this.data_api.read("Usuario",this.coockieservice.get("id_usuario") ).subscribe(data => {
  //     console.log(data,"este es el usuario");
  //   this.usuariobd = data.payload.data();
  //   this.tipo_u = this.usuariobd.tipo_usuario;
  //   this.coockieservice.set("tipoUser", this.tipo_u.toString());
  // }, (err: any) => {
  //     console.error(err);      
  //   }
  //   );

  // }

}
