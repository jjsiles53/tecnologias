import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AdminPrincComponent } from './components/admin-princ/admin-princ.component';
import { AuthGuard } from './guards/auth.guard';
import { AboutComponent } from './components/about/about.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { CompanyComponent } from './components/company/company.component';
import { ReadCompanyComponent } from './components/read-company/read-company.component';
import { EmpresasComponent } from './components/empresas/empresas.component';
import { GuiasComponent } from './components/guias/guias.component';
import { UserPanelComponent } from './components/user-panel/user-panel.component';
import { FavoritosComponent } from './components/favoritos/favoritos.component';
import { GestionarEmpresaComponent } from './components/gestionar-empresa/gestionar-empresa.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { TableroComponent } from './components/tablero/tablero.component';
import { CategoryComponent } from './components/category/category.component';
import { TablaclienteComponent } from './components/tablacliente/tablacliente.component';
import { TablaadministradorComponent } from './components/tablaadministrador/tablaadministrador.component';
import { TablaempresaComponent } from './components/tablaempresa/tablaempresa.component';
import {CategoriavistaclienteComponent} from './components/categoriavistacliente/categoriavistacliente.component';
import { ContactUserComponent } from './components/contact-user/contact-user.component';

const routes: Routes = [
  {path: '', component: TableroComponent},
  {path: 'home', component: HomeComponent,outlet:"homes"},
  {path: 'about', component: AboutComponent},
  {path: 'contacto', component: ContactoComponent},
  {path: 'company', component: ReadCompanyComponent},
  {path: 'verc', component: EmpresasComponent},
  {path: 'guias', component: GuiasComponent},
  {path: 'contactar', component: ContactUserComponent},
  {path: 'catcliente', component: CategoriavistaclienteComponent},

  {path: 'user/panel', component: UserPanelComponent, canActivate: [AuthGuard]},
  {path: 'favoritos', component: FavoritosComponent, canActivate: [AuthGuard]},
  {path: 'gestionar', component: GestionarEmpresaComponent, canActivate: [AuthGuard]},
  {path: 'cliente', component: ClientesComponent, canActivate: [AuthGuard]},
  {path: 'tablero', component: TableroComponent, canActivate: [AuthGuard]},
  {path: 'admin/company', component: CompanyComponent, canActivate: [AuthGuard]},
  {path: 'category', component: CategoryComponent, canActivate: [AuthGuard]},
  {path: 'cliente/tabla', component: TablaclienteComponent, canActivate: [AuthGuard]},
  {path: 'administrador/tabla', component: TablaadministradorComponent, canActivate: [AuthGuard]},
  {path: 'empresa/tabla', component: TablaempresaComponent, canActivate: [AuthGuard]},

  {path: 'adm', component: AdminPrincComponent},
  {path: '**', component: HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
