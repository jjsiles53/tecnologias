import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";
import { categoria } from '../modelos/categoria';
import { Empresa } from '../modelos/empresa';



@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {
  private categ: categoria ={id_categoria: 0, nombre_categoria: "", descripcion_categoria: ""} 

  constructor(protected http:HttpClient) {  }
  // CRUDE

  get_all(cadena): Observable<any>{
    return this.http.get<any>('/api/'+cadena);
  }
  add_f(cadena, emp: Empresa){
    return this.http.post('/api/' + cadena, emp );

  }
  add_all(object: any,cadena:any){
    // let json: any =JSON.stringify(object);
    console.log("json: ", object);
    console.log("cadena", cadena);
    // let headers = new HttpHeaders().set("Content.Type", "application/json");
    return this.http.post("/api/"+cadena, object,{responseType: 'text'});
  }
  dellete_all(cadena: any){
    return this.http.delete("/api/"+cadena);

  }
  update_all(cadena: any, object: any){

    return this.http.put("/api/"+cadena,object);  
    
  }
  // OTHERS
  get_emp_count(){
    return this.http.get('/api/empresa/count');
  }
  get_emp_token(id){
    return this.http.get('/api/empresa/token/'+id);
  }
  put_empresa_idpersona(id,token){
    
    
    return this.http.put('/api/empresa/actu_persona/'+id, token);
  }
  //Obtener las sucursales de una empresa
  get_suc_emp(id_empresa){
    return this.http.get('api/sucursal/'+id_empresa);
  }
  get_serv_id(id_serv){
    return this.http.get('api/servicio/'+id_serv);
  }
  //Obtener todos los servicios de una empresa, con nombre de la sucursal de donde son.
  get_servs_emp(id_empresa){
    return this.http.get('api/servicio/servempresa/'+id_empresa);
  }
  // GET --> Obtener empresa por id_usuario
  get_emp_id_user(id_usuario){
    return this.http.get('/api/empresa/id_usuario/'+id_usuario);
  }
  // GET --> Obtener Sucursal por id_empresa
  get_sucursal(id_empresa){
    console.log('estoy en la funcion', id_empresa);
    
    return this.http.get('/api/sucursal/id_empresa/'+id_empresa);
  }

  // GET --> Obtener un registro por su id
  getOneOf(cadena, id): Observable<any> {
    return this.http.get(`/api/${cadena}/${id}`);
  }
  
  // GET --> Todas las empresas con su nombre de categoria - por su id_categoria
  getEmpresasByCategory(id_cat): Observable<any> {
    return this.http.get('/api/empresa/categoria/' + id_cat);
  }


}
// https://cors-anywhere.herokuapp.com/