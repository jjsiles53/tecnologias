import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

import { auth } from 'firebase/app';
import { resolve } from 'url';
import { map } from 'rxjs/operators';
import { FirebaseApp } from '@angular/fire';
import { promise } from 'protractor';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { UserInterface } from '../modelos/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private afsAuth: AngularFireAuth, private firabase:FirebaseApp, private afs: AngularFirestore) { }

    loginUser(email: string, pass:string) {
      return new Promise((resolve, reject)=> {
        this.afsAuth.auth.signInWithEmailAndPassword(email, pass)
        .then(userData => resolve(userData),
        err => reject(err));
      });
    }
    registerUser(email, password): Promise<any> {
      return new Promise((resolve, reject) => {
        this.afsAuth.auth.createUserWithEmailAndPassword(email, password)
          .then(res => resolve(res)),
          err => reject(err);
      })
    }



    // registerUser(email:any, password:any) {
     
      
    //   this.firabase.auth().createUserWithEmailAndPassword(email, password).then(function(resp){
    //     console.log("logeado" , resp.user.uid);
    //     return resp.user.uid;
        
    //     // this.firabase.auth().currentUser.getIdToken(/* forceRefresh */ true);
        
        
    //   }).catch(function(error) {
    //     // Handle Errors here.
    //     var errorCode = error.code;
    //     var errorMessage = error.message;
    //     console.log(errorMessage);  
        
    //   });
    //  }
     get_usuario(){
      this.firabase.auth().currentUser.getIdToken(/* forceRefresh */ true).then(function(idToken) {
        // Send token to your backend via HTTPS
        // ...
        return idToken;
        
      }).catch(function(error) {
        // Handle error
      });
     }

    logoutUser() {
      return this.afsAuth.auth.signOut();
    }

    isAuth() {
      return this.afsAuth.authState.pipe(map(auth => auth));
    }

    pruebaUSer(userUid) {
      return this.afs.doc<UserInterface>(`Usuario/${userUid}`).valueChanges();
    }
}
