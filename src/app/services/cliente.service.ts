import { Injectable } from '@angular/core';
import{AngularFirestore} from '@angular/fire/firestore';
import{Observable} from 'rxjs';
import{map}from 'rxjs/operators';
import{Cliente}from '../modelos/cliente';
import { HttpClient, HttpHeaders } from '@angular/common/http';


import{AngularFireDatabase, AngularFireList} from '@angular/fire/database'
import { from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

    usuariolist:AngularFireList<any>;

    //tablacliente:AngularFireList<any>;
  constructor(private afs:AngularFirestore, private firebase:AngularFireDatabase) { } 

  //public getallcliente():Observable<Cliente[]>{
      //return this.afs.collection('Usuario')
      //.snapshotChanges()
      //.pipe(
        //map(actions =>
            //actions.map(a=>{
              //const data = a.payload.doc.data() as Cliente;
            //  const id = a.payload.doc.id;
          //    return {id, ... data};
        //    }) 
      //    )
    //  );
  //}

  public getOneCliente(id:Cliente):Observable<Cliente>{
    return this.afs.doc<Cliente>(`Usuario/${id}`).valueChanges();

  }

  //get_all(cadena): Observable<any>{
   // return this.http.get<any>('/api/'+cadena);
  //}

    getusuarioall()
    {
      return this.usuariolist=this.firebase.list('Usuario');
    }


}
