import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
@Injectable({
  providedIn: 'root'
})
export class DataApiService {

  constructor(private firestore: AngularFirestore) { }
  create(record, coleccion, id) {
    return this.firestore.collection(coleccion).doc(id).set(record);
  }
  read(coleccion,id) {
    return this.firestore.collection(coleccion).doc(id).snapshotChanges();
  }
}
