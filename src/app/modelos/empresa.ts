export interface Empresa {
    nombre_empresa: string;
    razon_social: string;
    nit_empresa: string;
    id_categoria: number;
    token: string;
}
