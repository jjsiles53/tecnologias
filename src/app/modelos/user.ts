export interface UserInterface {
    nombre: string; // Contiene la frase
    apellido: string;
    celular: string;
    clave: string;
    correo_electronico: string;
    tipo: number;
    token: string;
  }
