import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pipeEmpresas'
})
export class PipeEmpresasPipe implements PipeTransform {

  transform(value: any,args: any): any {
    const resultados = [];
    if (args === null || args === 'undefined')
    {
      return 'false';
    }
    for (const empresas of value)
    {
       if(empresas.nombre_empresa.toLowerCase().indexOf(args.toLowerCase())>-1)
       {
        resultados.push(empresas);       
       }       
    }
    return resultados;
  }

}
