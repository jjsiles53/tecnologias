import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'empCat'
})
export class EmpCatPipe implements PipeTransform {

  transform(value: any, ...args: any): any {
    // return null;
    console.log("pipe", value);
    const resultados = [];
    if (args === null || args === 'undefined')
    {
      return 'false';
    }
    for (const empresas of value)
    {
      
       if(empresas.id_categoria.indexOf(args)>-1)
       {
        resultados.push(empresas);       
       }       
    }
    return resultados;
  }

}
