import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCoinsComponent } from './modal-coins.component';

describe('ModalCoinsComponent', () => {
  let component: ModalCoinsComponent;
  let fixture: ComponentFixture<ModalCoinsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCoinsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCoinsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
