import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { CookieService } from 'ngx-cookie-service';
import swal from 'sweetalert';



@Component({
  selector: 'app-modal-coins',
  templateUrl: './modal-coins.component.html',
  styleUrls: ['./modal-coins.component.scss']
})
export class ModalCoinsComponent implements OnInit {
  servico: any;
  id_servicio:any;
  transaccion:any ={one_coin: "",id_usuario:"", id_empresa:""};

  constructor(private api_service:ApiServiceService, private cookie:CookieService) { }

  ngOnInit() {
    this.get_servicios();
  }
  get_servicios(){
    this.api_service.get_servs_emp(this.cookie.get("id_empresa")).subscribe(resp=>{
    this.servico = resp;

    },error=>{
      console.log(error);
      
    });
  }
  pagar_coins(){
    this.transaccion.id_empresa = this.cookie.get("id_empresa");
    this.api_service.get_serv_id(this.id_servicio).subscribe(resp=>{
      // console.log('servicio encontrado', resp);
      for (const key in resp) {
        if (resp.hasOwnProperty(key)) {
          const element = resp[key];
          this.transaccion.one_coin = element["one_coin_ps"];
          console.log("Este es mi transaccion", this.transaccion);
    

          this.api_service.add_all(this.transaccion,"one_coin_consumo/nuevo").subscribe(resp=>{
            console.log(resp);
            this.transaccion.one_coin = "";
            this.transaccion.id_usuario ="";
            this.transaccion.id_empresa="";
            
            // swal("Se Cargaron Los Coins", "Registro Exitoso!", "success");
            
          },error=>{
            console.log(error);
            // swal("no se cargaron los coins", "Registro Fallido!", "success");
      
          });
          // console.log("oneps", this.transaccion.one);                    
        }
      }
    });






  }
  dar_coins(){
    this.transaccion.id_empresa = this.cookie.get("id_empresa");
    
    this.api_service.get_serv_id(this.id_servicio).subscribe(resp=>{
      // console.log('servicio encontrado', resp);
      for (const key in resp) {
        if (resp.hasOwnProperty(key)) {
          const element = resp[key];
          this.transaccion.one_coin = element["one_coin_ps"];
          console.log("Este es mi transaccion", this.transaccion);
    

          this.api_service.add_all(this.transaccion,"one_coin_acum/nuevo").subscribe(resp=>{
            console.log(resp);
            this.transaccion.one_coin = "";
            this.transaccion.id_usuario ="";
            this.transaccion.id_empresa="";

            // swal("Se Cargaron Los Coins", "Registro Exitoso!", "success");
            
          },error=>{
            console.log(error);
            // swal("no se cargaron los coins", "Registro Fallido!", "success");
      
          });
          // console.log("oneps", this.transaccion.one);                    
        }
      }
    });
   



  }

}
