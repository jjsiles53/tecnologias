import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaadministradorComponent } from './tablaadministrador.component';

describe('TablaadministradorComponent', () => {
  let component: TablaadministradorComponent;
  let fixture: ComponentFixture<TablaadministradorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaadministradorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaadministradorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
