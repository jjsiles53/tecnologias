import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalinfoempresaComponent } from './modalinfoempresa.component';

describe('ModalinfoempresaComponent', () => {
  let component: ModalinfoempresaComponent;
  let fixture: ComponentFixture<ModalinfoempresaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalinfoempresaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalinfoempresaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
