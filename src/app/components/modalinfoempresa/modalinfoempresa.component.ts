import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modalinfoempresa',
  templateUrl: './modalinfoempresa.component.html',
  styleUrls: ['./modalinfoempresa.component.scss']
})
export class ModalinfoempresaComponent implements OnInit {
  title: string = 'AGM project';
  latitude: number;
  longitude: number;
  zoom:number;

  constructor() { }

  ngOnInit() {
    this.setCurrentLocation();
    
  

  }
 // Get Current Location Coordinates
 private setCurrentLocation() {
  if ('geolocation' in navigator) {
    navigator.geolocation.getCurrentPosition((position) => {
      this.latitude = position.coords.latitude;
      this.longitude = position.coords.longitude;
      this.zoom = 15;
    });
  }
}

}
