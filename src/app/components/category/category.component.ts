import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/services/api-service.service';
// import { ModalCategoriasComponent } from '../modal-categorias/modal-categorias.component';
import swal from 'sweetalert';
import * as $ from 'jquery';


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  categoriasbd:any;
  cat_update: any = {id_categoria: 0, nombre_categoria:"", descripcion_categoria:""};
  constructor(private api_service:ApiServiceService) { }

  ngOnInit() {
  this.get_categoria();

  }
  get_categoria(){
    this.api_service.get_all("categoria").subscribe(data => {
      console.log(data);
      
      this.categoriasbd = data;
    }, (err: any) => {
      console.error(err);      
    }
    );
    console.log(this.categoriasbd);

  }
  delete_categoria(id: any){
    
    var r = confirm("¿Esta seguro que quiere borrar este registro?");

    if (r == true) { 
     this.api_service.dellete_all("categoria/eliminar/"+id).subscribe(resp =>{
     swal("Categoria borrada!", "Registro eliminado!", "info");
     this.get_categoria();
  },error =>{
    swal("Categoria NO borrada!", "Ups algo salio mal!", "error");
    console.log(error);
    
  });

} 

    
  
  }
  
  update_data(id: any, nomb: any, desc: any){
    
    this.cat_update.id_categoria = id;
    this.cat_update.nombre_categoria = nomb;
    this.cat_update.descripcion_categoria = desc;
    console.log("categoria a modificar:",this.cat_update);
  }
  update_categoria(){
    this.api_service.update_all("categoria/modificar/"+ this.cat_update.id_categoria.toString(), this.cat_update).subscribe(resp=>{

      console.log(resp);
      swal("Categoria Modificada!", "Registro Modificado!", "success");
      $("[data-dismiss=modal]").trigger({ type: "click" });
      this.get_categoria();

      

    }, error=>{
      console.log(error);
      swal("Categoria NO Modificada!", "Ups ocurrio un error!", "error");

      
    });
  }


}
