import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { ApiServiceService } from 'src/app/services/api-service.service';
// import { ModalCategoriasComponent } from '../modal-categorias/modal-categorias.component';
import swal from 'sweetalert';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-categoriavistacliente',
  templateUrl: './categoriavistacliente.component.html',
  styleUrls: ['./categoriavistacliente.component.scss']
})
export class CategoriavistaclienteComponent implements OnInit {
  categoriasbd:any;
  cat_update: any = {id_categoria: 0, nombre_categoria:"", descripcion_categoria:""};
  constructor(private api_service:ApiServiceService,private route: ActivatedRoute, private enrutador:Router) { }
  empresas: any;
  params: any;
 // Obtener los parametros de la ruta
 getRouteParams() {
  this.route.queryParams.subscribe(QueryParams => {
    this.params = QueryParams;
    if (this.params.cat) {
      this.getEmpresasByCategory();
    } else {
      this.getAllEmpresas();
    }
  });
}

// Obtener todas las empresas sin filtros
getAllEmpresas() {
  this.api_service.get_all("empresa").subscribe(data => {
    this.empresas = data;
    console.log(this.empresas);
  }, err => console.log("error: " + err));
}

// Obtener todas las empresas de una categoria
getEmpresasByCategory() {
  this.api_service.getEmpresasByCategory(this.params.cat).subscribe(data => {
    this.empresas = data;
    console.log(this.empresas);
  }, err => console.log("error: " + err));
}
  ngOnInit() {
this.get_categoria();
this.getRouteParams();
}
  get_categoria(){
    this.api_service.get_all("categoria").subscribe(data => {
      console.log(data);
      
      this.categoriasbd = data;
    }, (err: any) => {
      console.error(err);      
    }
    );
    console.log(this.categoriasbd);

  }

  vermasEmpresa(id_empresa){
    localStorage.setItem("vermasEmpresa",id_empresa);
    this.enrutador.navigate(['verc']);
  }

}
