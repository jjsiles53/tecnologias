import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriavistaclienteComponent } from './categoriavistacliente.component';

describe('CategoriavistaclienteComponent', () => {
  let component: CategoriavistaclienteComponent;
  let fixture: ComponentFixture<CategoriavistaclienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoriavistaclienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriavistaclienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
