import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { CookieService } from 'ngx-cookie-service';




@Component({
  selector: 'app-gestionar-empresa',
  templateUrl: './gestionar-empresa.component.html',
  styleUrls: ['./gestionar-empresa.component.scss']
})
export class GestionarEmpresaComponent implements OnInit {

  title: string = 'AGM project';
  latitude: number;
  longitude: number;
  zoom: number;
  empresa: any;
  lat = [];
  lng = [];
  sucursal: any = { id_sucursal: 0, nombre_sucursal: 0, n_sucursal: "", direccion_sucursal: "", cordenada_x: 0, cordenada_y: 0, id_persona: "", id_empresa: 0 };
  servicio:any;


  constructor(private api_service: ApiServiceService, private coockie_service: CookieService) { }

  ngOnInit() {
    this.setCurrentLocation();
    this.get_empresa();
    this.get_sucursal();
    this.get_servicio();



  }
  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 15;
      });
    }
  }
  // GET empresa
  get_empresa() {
    var id_usaurio = this.coockie_service.get("id_usuario");
    this.api_service.get_emp_id_user(id_usaurio).subscribe(resp => {
      this.empresa = resp;
      for (const key in this.empresa) {
        if (this.empresa.hasOwnProperty(key)) {
          const element = this.empresa[key];
          // console.log("Element",element["id_empresa"]);
          this.coockie_service.set("id_empresa", element["id_empresa"]);


        }
      }
      // this.coockie_service.set("id_empresa",this.empresa.)

      console.log("esta es la empresa", resp);

    }, error => {
      console.log("Error", error);

    });
  }
  get_sucursal() {
    var id_empresa = this.coockie_service.get("id_empresa");
    this.api_service.get_sucursal(id_empresa).subscribe(resp => {
      this.sucursal = resp;
      console.log(this.sucursal);
      for (const key in this.sucursal) {
        if (this.sucursal.hasOwnProperty(key)) {
          const element = this.sucursal[key];
          this.lng.push(parseFloat(element["cordenada_x"]));
          this.lat.push(parseFloat(element["cordenada_y"]));
        }
      }
    }, error => {
      console.log(error);

    });
  }
  ag_serv(id_sucursal){
    this.coockie_service.set("id_sucursal", id_sucursal);

  }
  get_servicio(){
    this.api_service.get_servs_emp(this.coockie_service.get("id_empresa")).subscribe(resp=>{
    this.servicio = resp;
    console.log(resp);
    
    }, error=>{
      console.log(error);
      
    })
  }
}
