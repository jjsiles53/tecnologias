import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionarEmpresaComponent } from './gestionar-empresa.component';

describe('GestionarEmpresaComponent', () => {
  let component: GestionarEmpresaComponent;
  let fixture: ComponentFixture<GestionarEmpresaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionarEmpresaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionarEmpresaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
