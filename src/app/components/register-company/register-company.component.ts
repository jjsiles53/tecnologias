import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ApiServiceService } from 'src/app/services/api-service.service';

import { Observable } from 'rxjs/internal/Observable';


import { Router } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { async } from '@angular/core/testing';

// import { TableroComponent } from '../tablero/tablero.component';
import swal from 'sweetalert';
import { NgForm } from '@angular/forms';
import { interval } from 'rxjs';

import * as $ from 'jquery';
import { CompanyComponent } from '../company/company.component';


@Component({
  selector: 'app-register-company',
  templateUrl: './register-company.component.html',
  styleUrls: ['./register-company.component.scss']
})
export class RegisterCompanyComponent implements OnInit {

  constructor( private api_service:ApiServiceService, private router: Router, private storage: AngularFireStorage , private company: CompanyComponent) { }
  @ViewChild('imageUrl', {static: true}) imageUrl: ElementRef;
  @ViewChild('fundaUrl', {static: true}) fundaUrl: ElementRef;
  @ViewChild('btnClose', {static: false}) btnClose: ElementRef;

  
  uploadPercentImg: Observable<number>  
  uploadPercentFun: Observable<number>  
  urlImage: Observable<string>  
  urlFunda: Observable<string>
  categoria : any;
  token: string = "";
  empresaRegistro: any={nombre_empresa: "", razon_social: "", nit_empresa: "", fundaempresa: "", id_categoria :0, logo_empresa: "", token:""};

  ngOnInit() {
    this.get_categoria();
    const secondsCounter = interval(5000);
    secondsCounter.subscribe(n =>
      this.get_categoria()  );
    
    
    this.generateToken();
   
  }
  get_categoria(){
    this.api_service.get_all("categoria").subscribe(data => {
      // console.log(data);
      
      this.categoria = data;
    }, (err: any) => {
      console.error(err);      
    }

    );   
  }
  
  
  onUpLoadImg(e) {
    console.log('subir', e.target.files[0]);
    const id = Math.random().toString(36).substring(2);
    const file = e.target.files[0];
    const filePath = `imagenEmpresa/img_${id}`;
    const ref = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    this.uploadPercentImg = task.percentageChanges();
  
    task.snapshotChanges().pipe( finalize(() => this.urlImage = ref.getDownloadURL())).subscribe();
    this.reload();
     
  }

  onUpLoadFun(fund) {
    console.log('subir', fund.target.files[0]);
    const id = Math.random().toString(36).substring(2);
    const file = fund.target.files[0];
    const filePath = `fundaempresa/funda_${id}`;
    const ref = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    this.uploadPercentFun = task.percentageChanges();
    task.snapshotChanges().pipe( finalize(() => this.urlFunda = ref.getDownloadURL())).subscribe();
    this.reload();
    
  }
  reload(){
    this.urlFunda.subscribe(res=>{
      this.empresaRegistro.fundaempresa = res;
    });
    this.urlImage.subscribe(res=>{
      this.empresaRegistro.logo_empresa = res;
    });
    // console.log(this.empresaRegistro.fundaempresa,this.empresaRegistro.logo_empresa);
    
  }
  // Registrar Empresa
  registrar_emrpesa(formRegister: NgForm){

    let auxImg: string, auxFun: string;

    auxImg = this.imageUrl.nativeElement.value;
    formRegister.value.urlImage = auxImg;
    auxFun = this.fundaUrl.nativeElement.value;
    formRegister.value.urlFunda = auxFun;
    

    this.empresaRegistro.logo_empresa = formRegister.value.urlImage;
    this.empresaRegistro.fundaempresa = formRegister.value.urlFunda;

  
    console.log("Empresa",this.empresaRegistro);
    
     this.api_service.add_all(this.empresaRegistro,"empresa/nuevo").subscribe(response=>{
       console.log("Insercion Completada", response);
       this.company.ngOnInit();
       
       swal("Empresa Registrada!", "Registro Exitoso!", "success");
       $("[data-dismiss=modal]").trigger({ type: "click" });
     },
     error =>{
       console.log(error);
       swal("Empresa NO Registrada!", "Ups algo salio mal!", "error");
     });
     //this.empresaRegistro.resetForm();
     //this.btnClose.nativeElement.click();
    
  }
  // Generado de TOKEN
  generateToken(){
    // console.log("token");
    var numero: number;
    var cadena: string;
    
    for (let i = 0; i <6; i++) {
      numero = Math.floor(Math.random() * (91 - 65) + 65);
      let x = String.fromCharCode(numero);
      this.token = this.token + x;
      // console.log(x);      
    }
    this.empresaRegistro.token = this.token;
    // console.log(this.token);
    


  }
}

