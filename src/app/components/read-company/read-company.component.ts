import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { PipeEmpresasPipe } from 'src/app/pipes/pipe-empresas.pipe';
import { Router } from '@angular/router';
Router
@Component({
  selector: 'app-read-company',
  templateUrl: './read-company.component.html',
  styleUrls: ['./read-company.component.scss']
})
export class ReadCompanyComponent implements OnInit {

  constructor(private api_service:ApiServiceService, private enrutador:Router) { }
  empresasdb:any;
  searchbox : any;
  ngOnInit() {
    this.searchbox = localStorage.getItem("Busqueda_empresa");
    console.log(this.searchbox);
    if (this.searchbox == 'undefined' || this.searchbox == null)
    {
      this.searchbox = '';
    }
    localStorage.removeItem("Busqueda_empresa");
    
    this.api_service.get_all("empresa").subscribe(data => {
      console.log(data);
      
      this.empresasdb = data;
    }, (err: any) => {
      console.error(err);      
    }
    );
    console.log('empresasdb',this.empresasdb);

  }

  vermasEmpresa(id_empresa){
    localStorage.setItem("vermasEmpresa",id_empresa);
    this.enrutador.navigate(['verc']);
  }

}
