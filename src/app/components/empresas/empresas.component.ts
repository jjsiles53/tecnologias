import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-empresas',
  templateUrl: './empresas.component.html',
  styleUrls: ['./empresas.component.scss']
})
export class EmpresasComponent implements OnInit {
  
  constructor(private api_service: ApiServiceService, private coockie_service: CookieService) { }
  sucursal: any;
  id_empresa: any;
  latitude: number;
  longitude: number;
  zoom: number;
  lat = [];
  lng = [];
  empresadb;
  sucursalesdb;
  serviciosdb;
  ngOnInit() {
    this.id_empresa = localStorage.getItem("vermasEmpresa");
    console.log(this.id_empresa);
    // localStorage.removeItem("vermasEmpresa");
    //OBTENIENDO DATOS DE LA BD
    //Obteniendo empresa
    this.api_service.get_all("empresa/" + this.id_empresa).subscribe(data => {
      console.log("empresa", data);
      
      this.empresadb = data;
    }, (err: any) => {
      console.error(err);
    }
    );
    //Obteniendo sucursales
    this.api_service.get_all("sucursal/" + this.id_empresa).subscribe(data => {
      console.log(data);
      
      this.sucursalesdb = data;
    }, (err: any) => {
      console.error(err);
    }
    );
    //obteniendo servicios de una empresa, con la sucursal a la que pertenecen
    this.api_service.get_servs_emp(this.id_empresa).subscribe(data => {
      // console.log("estoy aqui", data);

      this.serviciosdb = data;
    }, (err: any) => {
      console.error(err);
    }
    );
    this.get_sucursal();
  }
  get_sucursal() {
    console.log("estoy aquiiiiiii");
    
    var id_empresa = localStorage.getItem("vermasEmpresa");
    this.api_service.get_sucursal(id_empresa).subscribe(resp => {
      this.sucursal = resp;
      // console.log("sucursales", resp);
      for (const key in this.sucursal) {
        if (this.sucursal.hasOwnProperty(key)) {
          const element = this.sucursal[key];
          this.lng.push(parseFloat(element["cordenada_x"]));
          this.lat.push(parseFloat(element["cordenada_y"]));
         
          
        }
      }
    }, error => {
      console.log(error);

    });
    console.log(this.lat, this.lng);
  }


}
