import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { CategoryComponent } from '../category/category.component';
import swal from 'sweetalert';
import * as $ from 'jquery';



@Component({
  selector: 'app-modal-categorias',
  templateUrl: './modal-categorias.component.html',
  styleUrls: ['./modal-categorias.component.scss']
})
export class ModalCategoriasComponent implements OnInit {

  constructor( private api_service:ApiServiceService, public category: CategoryComponent ) { }
  categoriaRegistro: any = {nombre_categoria: "",descripcion_categoria : ""}
  
  ngOnInit() { 
    
  }
    // Registrar Ctegoria
    registrar_categoria(){
      console.log(this.categoriaRegistro);
      this.api_service.add_all(this.categoriaRegistro,"categoria/nuevo").subscribe(response=>{
        console.log("Insercion Completada", response);
        this.categoriaRegistro =  {nombre_categoria:"",descripcion_categoria : ""};
        this.get_categoria();
        swal("Categoria agregada!", "Registro Agregado correctamente!", "success");
        $("[data-dismiss=modal]").trigger({ type: "click" });


        
      },
      error =>{
        swal("Categoria NO agregada!", "Ocurrio un error!", "error");

        console.log(error);
        
        
      });
      
    }
    
    get_categoria(){
      this.api_service.get_all("categoria").subscribe(data => {
        console.log(data);
        
        this.category.categoriasbd = data;
      }, (err: any) => {
        console.error(err);      
      }
      );
      console.log(this.category.categoriasbd);
  
    }

   
    
}
