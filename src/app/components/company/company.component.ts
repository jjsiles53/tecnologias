import { Component, OnInit } from '@angular/core';
import { RegisterCompanyComponent } from '../register-company/register-company.component';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {

  constructor(private api_service: ApiServiceService, private route: ActivatedRoute, private cookie: CookieService) { }

  empresas: any;
  params: any;

  ngOnInit() {
    this.getRouteParams();
  }

  // Obtener los parametros de la ruta
  getRouteParams() {
    this.route.queryParams.subscribe(QueryParams => {
      this.params = QueryParams;
      if (this.params.cat) {
        this.getEmpresasByCategory();
      } else {
        this.getAllEmpresas();
      }
    });
  }

  // Obtener todas las empresas sin filtros
  getAllEmpresas() {
    this.api_service.get_all("empresa").subscribe(data => {
      this.empresas = data;
      console.log(this.empresas);
    }, err => console.log("error: " + err));
  }

  // Obtener todas las empresas de una categoria
  getEmpresasByCategory() {
    this.api_service.getEmpresasByCategory(this.params.cat).subscribe(data => {
      this.empresas = data;
      console.log(this.empresas);
    }, err => console.log("error: " + err));
  }

}
