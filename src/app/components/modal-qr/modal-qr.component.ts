import { Component, OnInit } from '@angular/core';
import { QRCodeModule } from 'angularx-qrcode';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-modal-qr',
  templateUrl: './modal-qr.component.html',
  styleUrls: ['./modal-qr.component.scss']
})
export class ModalQrComponent implements OnInit {

  public myAngularxQrCode: string = null;

  constructor(private qr: QRCodeModule, private cookie: CookieService) { 
    this.myAngularxQrCode = this.cookie.get('id_usuario');
  }

  ngOnInit() {
  }

}
