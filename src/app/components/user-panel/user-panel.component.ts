import { Component, OnInit } from '@angular/core';
import { QRCodeModule } from 'angularx-qrcode';
import { CookieService } from 'ngx-cookie-service';
import { ApiServiceService } from 'src/app/services/api-service.service';



@Component({
  selector: 'app-user-panel',
  templateUrl: './user-panel.component.html',
  styleUrls: ['./user-panel.component.scss']
})
export class UserPanelComponent implements OnInit {
  public myAngularxQrCode: string = null;
  
  constructor(private cock:CookieService, private api_service:ApiServiceService) { 
    this.myAngularxQrCode = this.cock.get('id_usuario');
  }
  resultadobd:any;
  
  ngOnInit() {
    this.get_empresas_coins();
  }

  get_empresas_coins(){
    this.api_service.get_all("one_coin_acum/obtener/"+this.cock.get('id_usuario')).subscribe(data=>{
      this.resultadobd = data;
      console.log(data);
      
    }, error=>{
      console.log(error);
      
    });

  }


}
