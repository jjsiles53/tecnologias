import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import swal from 'sweetalert';
import { DataApiService } from 'src/app/services/data-api.service';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent implements OnInit {

  constructor(private authService: AuthService, private afsAuth: AngularFireAuth,private api_service: ApiServiceService, private router: Router, private data_api:DataApiService, private cookie: CookieService) { }

  categories: any;
  public tipo_u: string;
  public usuariobd:any;

  ngOnInit() {
    this.getCategories();
    this.tipo_u = this.cookie.get("tipoUser");
    console.log("cookies", this.tipo_u);
    
  }

  // Obtener todas las categorias
  getCategories() {
    this.api_service.get_all("categoria").subscribe(data => {
      this.categories = data;
    }, err => console.log("error: " + err))
  }

  // Redirigir al link de admin/empresas
  goToEmpresas() {
    this.router.navigate(['/admin/company']);
  }

  onLogout() {

    var r = confirm("¿Esta seguro que quieres salir?");

    if (r == true) { 
      this.cookie.deleteAll();
      localStorage.clear();
    this.authService.logoutUser();
    this.router.navigate(['home']);
    }else{
      swal("deslogeo Cancelado!", "Bienvenido otra vez!", "info");
    }

  }

  
}
