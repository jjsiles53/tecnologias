import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPrincComponent } from './admin-princ.component';

describe('AdminPrincComponent', () => {
  let component: AdminPrincComponent;
  let fixture: ComponentFixture<AdminPrincComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPrincComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPrincComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
