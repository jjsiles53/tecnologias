import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalagregarserviciosComponent } from './modalagregarservicios.component';

describe('ModalagregarserviciosComponent', () => {
  let component: ModalagregarserviciosComponent;
  let fixture: ComponentFixture<ModalagregarserviciosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalagregarserviciosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalagregarserviciosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
