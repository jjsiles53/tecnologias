import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { ApiServiceService } from 'src/app/services/api-service.service';
import swal from 'sweetalert';
import * as $ from 'jquery';
import { GestionarEmpresaComponent } from '../gestionar-empresa/gestionar-empresa.component';




@Component({
  selector: 'app-modalagregarservicios',
  templateUrl: './modalagregarservicios.component.html',
  styleUrls: ['./modalagregarservicios.component.scss']
})
export class ModalagregarserviciosComponent implements OnInit {
  servicio: any = { nombre_servicio:"", descripcion_servicio:"", precio_servicio:"", one_coin_ps: "", one_coin_promo:"", id_subcategoria:"", id_sucursal:""};
  sub_categoria: any;

  constructor(private coockie_service:CookieService, private api_service:ApiServiceService, private gest:GestionarEmpresaComponent) { }

  ngOnInit() {
    this.get_subcategoria();
  }
  get_subcategoria(){
    this.api_service.get_all("subcategoria").subscribe((resp:any)=>{
      console.log("este es mi subservicio",resp);
      
      this.sub_categoria = resp;

    },error=>{
      console.log(error);
      

    });

  }
  registrar_servicio(){
    this.servicio.id_sucursal = this.coockie_service.get("id_sucursal");
    console.log("Este es el servicio",this.servicio);
    
    this.api_service.add_all(this.servicio,"servicio/nuevo").subscribe(resp=>{
      console.log(resp);
      this.coockie_service.delete("id_sucursal");
      this.gest.get_servicio();
      swal("Servicio Registrado Correctamente", "Registro Completado!", "info");
      $("[data-dismiss=modal]").trigger({ type: "click" });
  
      
    },error=>{
      console.log(error);
      
    });
  


  }




}
