import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { interval } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-tablero',
  templateUrl: './tablero.component.html',
  styleUrls: ['./tablero.component.scss']
})
export class TableroComponent implements OnInit {

  cont:any;
  vitacorabd:any;

  constructor(private api_service: ApiServiceService, private cookie: CookieService, private router: Router) { }

  
  ngOnInit() {
    
      this.get_countEmp()
      const secondsCounter = interval(5000);
      secondsCounter.subscribe(n =>
        this.get_countEmp()  );
      this.get_vitacora();
    
  }


  get_countEmp(){
    this.api_service.get_emp_count().subscribe(resp=>{
      this.cont = resp;
      console.log("Numero de Empresas"+ resp);
      
    }, error=>{
      console.log(error);
      
    });

  }

  get_vitacora(){
    this.api_service.get_all("vitacora").subscribe(data=>{
      this.vitacorabd = data;
      console.log("Vitacora"+ data);
      
    }, error=>{
      console.log(error);
      
    });

  }


}
