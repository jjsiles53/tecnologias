import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {
  searchbox: "";
  constructor(private enrutador:Router) { }

  ngOnInit() {
  }
  busqueda(){
    console.log(this.searchbox);
    localStorage.setItem("Busqueda_empresa",this.searchbox);
    this.enrutador.navigate(['company']);
  }

}
