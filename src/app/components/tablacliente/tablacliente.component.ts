import { Component, OnInit } from '@angular/core';
import{ClienteService} from '../../services/cliente.service';
import{Cliente} from '../../modelos/cliente'
import { element } from 'protractor';
import { ActivatedRoute } from '@angular/router';
import { Observable, from, Subscriber } from 'rxjs';
import { DataApiService } from 'src/app/services/data-api.service';
import { CookieService } from 'ngx-cookie-service';
import{AngularFirestore} from '@angular/fire/firestore';
 

@Component({
  selector: 'app-tablacliente',
  templateUrl: './tablacliente.component.html',
  styleUrls: ['./tablacliente.component.scss']
})
export class TablaclienteComponent implements OnInit {
  usuariobd:any;
  clientes: any[]= new Array<any>();
  //usuariolist: Cliente[];
  //public cliente$:Observable<Cliente>;
  //clienteList:Cliente[];
  constructor(private route:ActivatedRoute, private clienteSvc:ClienteService,private data_api:DataApiService,
    private coockieservice: CookieService, private db:AngularFirestore  ) { }

  ngOnInit() {
   // const idCliente= this.route.snapshot.params.id;
   // this.cliente$=this.clienteSvc.getOneCliente(idCliente);
   
  // this.get_cliente();

   //data.map(e => {
    //return {
      //id: e.payload.doc.id,
      //nombre_usuario: e.payload.doc.data()['nombre_usuario'],
      //apellido_usuario: e.payload.doc.data()['apellido_usuario'],
      //email_usuario: e.payload.doc.data()['email_usuario'],
      //telefono: e.payload.doc.data()["telefono"],
      //direccion: e.payload.doc.data()["direccion"],
      //clave: e.payload.doc.data()['clave'],
      //especialidad: e.payload.doc.data()['especialidad'],
     // categoria: e.payload.doc.data()['categoria']

   // };
 // })
 //otra funcion
   this.db.collection('Usuario').valueChanges().subscribe((resultado)=>{
     //debugger
     this.clientes=resultado;
     console.log(resultado);
   });
  }
  get_cliente(){
    this.data_api.read("Usuario",this.coockieservice.get("id_usuario") ).subscribe(data => {
      console.log(data,"este es el usuario");
    this.usuariobd = data.payload.data();
    console.log(this.usuariobd);
  }, (err: any) => {
      console.error(err);      
    }
    );
    console.log(this.usuariobd);

  }

  
}
