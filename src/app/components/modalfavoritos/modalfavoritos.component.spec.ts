import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalfavoritosComponent } from './modalfavoritos.component';

describe('ModalfavoritosComponent', () => {
  let component: ModalfavoritosComponent;
  let fixture: ComponentFixture<ModalfavoritosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalfavoritosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalfavoritosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
