import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaempresaComponent } from './tablaempresa.component';

describe('TablaempresaComponent', () => {
  let component: TablaempresaComponent;
  let fixture: ComponentFixture<TablaempresaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaempresaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaempresaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
