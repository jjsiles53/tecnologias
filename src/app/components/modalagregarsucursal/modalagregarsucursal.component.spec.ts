import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalagregarsucursalComponent } from './modalagregarsucursal.component';

describe('ModalagregarsucursalComponent', () => {
  let component: ModalagregarsucursalComponent;
  let fixture: ComponentFixture<ModalagregarsucursalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalagregarsucursalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalagregarsucursalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
