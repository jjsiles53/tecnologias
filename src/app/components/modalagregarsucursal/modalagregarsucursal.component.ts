import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { ApiServiceService } from 'src/app/services/api-service.service';
import swal from 'sweetalert';
import * as $ from 'jquery';
import { GestionarEmpresaComponent } from '../gestionar-empresa/gestionar-empresa.component';



@Component({
  selector: 'app-modalagregarsucursal',
  templateUrl: './modalagregarsucursal.component.html',
  styleUrls: ['./modalagregarsucursal.component.scss']
})
export class ModalagregarsucursalComponent implements OnInit {

  title: string = 'AGM project';
  latitude: number;
  longitude: number;
  zoom:number;
  sucursal: any = {nombre_sucursal: "", n_sucursal: "", direccion_sucursal: "", cordenada_x:"", cordenada_y:"", id_persona:"", id_empresa:""};
  empre: any;


  constructor(private coockie_service: CookieService, private api_service:ApiServiceService, private gest_emp: GestionarEmpresaComponent) { }

  ngOnInit() {
    this.setCurrentLocation();
     
  
    
  

  }
 // Get Current Location Coordinates
 private setCurrentLocation() {
  if ('geolocation' in navigator) {
    navigator.geolocation.getCurrentPosition((position) => {
      this.latitude = position.coords.latitude;
      this.longitude = position.coords.longitude;
      this.zoom = 15;
    });
  }
}
mapClick(event){
  this.latitude = event.coords.lat;
  this.longitude = event.coords.lng;  
  console.log(this.latitude, this.longitude);
}
registrar_sucursal(){
  this.sucursal.cordenada_x = this.longitude;
  this.sucursal.cordenada_y = this.latitude;
  this.sucursal.id_persona = this.coockie_service.get("id_usuario");
  this.sucursal.id_empresa = this.coockie_service.get("id_empresa");
  console.log(this.sucursal);
  this.api_service.add_all(this.sucursal,"sucursal/nuevo").subscribe(resp=>{
    console.log(resp);
    this.gest_emp.get_sucursal();
    swal("Sucursal Registrada Correctamente", "Vuelva pronto!", "info");
    $("[data-dismiss=modal]").trigger({ type: "click" });

    
  },error=>{
    console.log(error);
    
  });

}

}
