import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import swal from 'sweetalert';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private authService: AuthService, private afsAuth: AngularFireAuth, private router: Router) { }
  title = 'new';

  public islogged: boolean = false;

  ngOnInit() {
    this.getCurrentUser();
  }

  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if(auth) {
        console.log('user logged');
        this.islogged = true;
      }
        else {
          console.log('Not user logged');
          this.islogged = false;
        }
    })
  }

  onLogout() {
    var r = confirm("¿Esta seguro que quieres salir?");

    if (r == true) { 
      swal("Se deslogeo correctamente!", "Vuelva pronto!", "info");
    this.authService.logoutUser();
    this.router.navigate(['home']);
    }else{
      swal("deslogeo Cancelado!", "Bienvenido otra vez!", "info");

    }
  }
}
