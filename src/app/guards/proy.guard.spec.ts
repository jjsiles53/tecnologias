import { TestBed, async, inject } from '@angular/core/testing';

import { ProyGuard } from './proy.guard';

describe('ProyGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProyGuard]
    });
  });

  it('should ...', inject([ProyGuard], (guard: ProyGuard) => {
    expect(guard).toBeTruthy();
  }));
});
