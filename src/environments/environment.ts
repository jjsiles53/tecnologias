// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyAF1w8Y5uQfyyBS2iGLuHLOKzgmItCbgsw",
    authDomain: "prueba-435d6.firebaseapp.com",
    databaseURL: "https://prueba-435d6.firebaseio.com",
    projectId: "prueba-435d6",
    storageBucket: "prueba-435d6.appspot.com",
    messagingSenderId: "489746768815",
    appId: "1:489746768815:web:b5448d83c5d66a8939c55a",
    measurementId: "G-TQ7G3S4FG1"
  }
   
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
